package com.example.logisticah;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ListaActivity extends ListActivity implements
        Response.Listener<JSONObject>,Response.ErrorListener
{
    DBEstados dbEstados;
    Estados estado;
    private final Context context = this;
    private ProcesosPHP php = new ProcesosPHP();
    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;
    Device device;
    ListView listView;
    private ArrayAdapter<Estados> adapter;
    private ArrayList<Estados> listaContactos;
    ArrayList<Estados> copyContactoss = new ArrayList<>();
    private String serverip = "https://cursoandroidupsin2019.000webhostapp.com/SW/";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);
        Button btnNuevo = (Button)findViewById(R.id.btnNuevo);
        dbEstados = new DBEstados(this);
        estado = new Estados();
        request = Volley.newRequestQueue(context);
        listaContactos=new ArrayList<Estados>();
        php.setContext(context);
       // ArrayList<Estados> Estados = dbEstados.obtener_(Device.getSecureId(this));
       // MyArrayAdapter adapter = new MyArrayAdapter(this, R.layout.layout_estados, Estados);
        //setListAdapter(adapter);
        btnNuevo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
        consultarTodosWebService();
    }
    class MyArrayAdapter extends ArrayAdapter<Estados>
    {
        Context context;
        int textViewResourceId;
        ArrayList<Estados> objects;
        public MyArrayAdapter(Context context, int textViewResourceId, ArrayList<Estados> objects)
        {
            super(context, textViewResourceId, objects);
            this.context = context;
            this.textViewResourceId = textViewResourceId;
            this.objects = objects;
        }
        public View getView(final int position, View convertView, ViewGroup viewGroup)
        {
            LayoutInflater layoutInflater =(LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewResourceId, null);
            TextView lblNombre =(TextView)view.findViewById(R.id.lblNombreEstado);
            TextView lblID =(TextView)view.findViewById(R.id.lblID);
            Button modificar = (Button)view.findViewById(R.id.btnModificar);
            Button btnBorrar = (Button)view.findViewById(R.id.btnBorrar);
            lblNombre.setText(objects.get(position).getNombre());
            lblID.setText(""+objects.get(position).getId());
            btnBorrar.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Log.e("",""+objects.get(position).getId());
                    dbEstados.delete(objects.get(position).getId());
                    php.borrarEstadoWebService(objects.get(position).getId());
                    objects.remove(position);
                    notifyDataSetChanged();
                }
            });
            modificar.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v) {
                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("contacto", objects.get(position));
                    Intent i = new Intent();
                    i.putExtras(oBundle);
                    setResult(Activity.RESULT_OK, i);
                    finish();
                }
            });
            return view;
        }
    }

    public void consultarTodosWebService(){
        Log.e("",""+copyContactoss.size());
        String url = serverip +"wsConsultarTodos.php?idMovil="+ Device.getSecureId(this);
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }
    @Override
    public void onErrorResponse(VolleyError error) {
    }
    @Override
    public void onResponse(JSONObject response) {
        JSONArray json = response.optJSONArray("estado");
        try {
            for(int i=0;i<json.length();i++)
            {
                estado = new Estados();
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);
                estado.setId(jsonObject.optInt("id"));
                estado.setNombre(jsonObject.optString("nombre"));
                estado.setIdMovil(jsonObject.optString("id_movil"));
                listaContactos.add(estado);
            }
            copyContactoss.addAll(listaContactos);
            MyArrayAdapter adapter = new MyArrayAdapter(context,R.layout.layout_estados,listaContactos);
            setListAdapter(adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}