package com.example.logisticah;


import android.content.Context;
import android.util.Log;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONObject;
import java.util.ArrayList;

public class ProcesosPHP implements Response.Listener<JSONObject>,Response.ErrorListener
{
    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;
    private ArrayList<Estados> estados = new ArrayList<Estados>();
    private String serverip = "https://cursoandroidupsin2019.000webhostapp.com/SW/";

    public void setContext(Context context)
    {
        request = Volley.newRequestQueue(context);
    }

    public void insertarEstadoWebService(Estados c)
    {
        Log.e("","asd");
        String url = serverip + "wsRegistro.php?"
                + "nombre="+c.getNombre()
                + "&id_movil="+c.getIdMovil()
                + "&status= 1";
        url = url.replace(" ","%20");
        Log.e("",""+url);
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }

    public void actualizarEstadoWebService(Estados c,int id){
        String url = serverip + "wsActualizar.php?id="+id+"&id_movil="+c.getIdMovil()+"&nombre="+c.getNombre()+"&status="+c.getStatus();
        url = url.replace(" ","%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }

    public void borrarEstadoWebService(int id){
        String url = serverip + "wsEliminar.php?id="+id;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.i("ERROR",error.toString());
    }

    @Override
    public void onResponse(JSONObject response) {

    }
}
