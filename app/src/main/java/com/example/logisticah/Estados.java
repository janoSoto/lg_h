package com.example.logisticah;

import java.io.Serializable;

public class Estados implements Serializable
{

        private int id;
        private String nombre;
        private int status;
        private String idMovil;

        public Estados() {
            setId(0);
            setNombre("");
            setStatus(0);
            setIdMovil("");
        }

        public Estados(int id, String nombre, int status,String idMovil) {

            this.id = id;
            this.nombre = nombre;
            this.status = status;
            this.setIdMovil(idMovil);

        }


        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getIdMovil() {
            return idMovil;
        }

        public void setIdMovil(String idMovil) {
            this.idMovil = idMovil;
        }
}

