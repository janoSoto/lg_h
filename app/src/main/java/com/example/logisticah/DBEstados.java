package com.example.logisticah;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

    public class DBEstados extends SQLiteOpenHelper {
        SQLiteDatabase db;

        public  DBEstados(Context context){
            super(context,SQLite_BD.DATABASE_NAME,null,SQLite_BD.DATABASE_VERSION);
        }
        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            sqLiteDatabase.execSQL(SQLite_BD.TABLE_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
            sqLiteDatabase.execSQL(SQLite_BD.Estado_TABLE_DROP);
            this.onCreate(sqLiteDatabase);
        }

        public void insert(Object o)
        {
            Estados es = new Estados();
            es = (Estados) o;
            SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
            sqLiteDatabase.isOpen();
            ContentValues values = new ContentValues();
            values.put(SQLite_BD.Estado.ID_Nombre,((Estados) o).getNombre());
            values.put(SQLite_BD.Estado.ID_MOVIL,((Estados) o).getIdMovil());
            values.put(SQLite_BD.Estado.STATUS,1);
            sqLiteDatabase.insert(SQLite_BD.Estado.TABLE_NAME,null,values);
            sqLiteDatabase.close();
        }

        public void update(Object o){
            Estados es = new Estados();
            es=(Estados) o;
            SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
            sqLiteDatabase.isOpen();
            ContentValues values = new ContentValues();
            values.put(SQLite_BD.Estado.ID_Nombre,es.getNombre());
            values.put(SQLite_BD.Estado.STATUS,1);

            sqLiteDatabase.update(SQLite_BD.Estado.TABLE_NAME,values, SQLite_BD.Estado.ID + "="+es.getId(),null);
            sqLiteDatabase.close();

        }

        public void delete(int id)
        {
            String  query = "Update " + SQLite_BD.Estado.TABLE_NAME +" set " + SQLite_BD.Estado.STATUS + " = "+0+" where id = "+id;
            SQLiteDatabase db = this.getReadableDatabase();
            db.execSQL(query);
        }

        public Estados readEstado(Cursor cursor){
            Estados estado = new Estados();
            estado.setId(cursor.getInt(0));
            estado.setNombre(cursor.getString(1));
            estado.setStatus(cursor.getInt(2));
            return estado;
        }

        public Estados getEstado(int id,String id_M){
            Estados es=new Estados();
            String  query="SELECT * from " + SQLite_BD.Estado.TABLE_NAME+" WHERE "+SQLite_BD.Estado.ID+"="+id+" and "+SQLite_BD.Estado.ID_MOVIL+"="+id_M;
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor=db.rawQuery(query,null);
            if(cursor==null){

            }else {
                if (cursor.moveToFirst()){
                    es=new Estados();
                    es.setId(Integer.parseInt(cursor.getString(0)));
                    es.setNombre(cursor.getString(1));
                }
            }
            return es;
        }
        public ArrayList obtener_(String id_M){
            Estados es = new Estados();
            String  query="SELECT * from " + SQLite_BD.Estado.TABLE_NAME + " WHERE " + SQLite_BD.Estado.STATUS + " = 1"+" and "+SQLite_BD.Estado.ID_MOVIL+"='"+id_M+"'";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor=db.rawQuery(query,null);
            ArrayList<Estados>list=new ArrayList();
            if(cursor==null)
            {

            }else
                {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()){
                    es=new Estados();
                    es.setId(Integer.parseInt(cursor.getString(0)));
                    es.setNombre(cursor.getString(1));
                    es.setStatus(2);
                    list.add(es);
                    cursor.moveToNext();
                }
            }
            cursor.close();
            return list;
        }
        public boolean verificarLista()
        {
            boolean a=false;
            String query = "SELECT * FROM " + SQLite_BD.Estado.TABLE_NAME;
            SQLiteDatabase db = this.getReadableDatabase();
            db.rawQuery(query,null);
            Cursor cursor=db.rawQuery(query,null);
            if(!cursor.moveToNext())
            {
                a=false;
            }
            else
            {
                a= true;
            }
            return  a;
        }
}
