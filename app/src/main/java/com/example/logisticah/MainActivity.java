package com.example.logisticah;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;

public class MainActivity extends AppCompatActivity {

    final ArrayList<Estados> estados = new ArrayList<Estados>();
    EditText txtSerie;
    Button btnAgregar;
    Button btnLimpiar;
    Button btnCerrar;
    Button btnListar;
    Estados estado;
    Estados saveEstado;
    int savedIndex;
    DBEstados dbEstados;
    ProcesosPHP sqlPhp;

    public void limpiar(){
        saveEstado = null;
        txtSerie.setText("");
    }

    protected void onActivityResult( int requestCode, int resultCode, Intent intent){
        super.onActivityResult(requestCode,resultCode,intent);
        if(intent != null){

            Bundle oBundle  = intent.getExtras();
            saveEstado = (Estados)oBundle.getSerializable("contacto");
            savedIndex = oBundle.getInt("index");
            txtSerie.setText(saveEstado.getNombre());
        }
        else{
            limpiar();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtSerie = (EditText)findViewById(R.id.txtSerie);
        btnAgregar = (Button) findViewById(R.id.agregar);
        btnLimpiar =(Button)findViewById(R.id.limpiar);
        dbEstados = new DBEstados(this);
        estado = new Estados();
        btnListar = (Button)findViewById(R.id.listar);
        btnCerrar = (Button)findViewById(R.id.cerrar);
        this.sqlPhp = new ProcesosPHP();
        sqlPhp.setContext(this);

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtSerie.getText().toString().equals(""))
                {
                    Toast.makeText(MainActivity.this, "Llene todos los campos!", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Estados nEstado = new Estados();
                    DBEstados dbEstados=new DBEstados(MainActivity.this);
                    int index= estados.size();
                    if(saveEstado != null)
                    {
                        nEstado.setId(saveEstado.getId());
                        nEstado.setNombre(txtSerie.getText().toString());
                        nEstado.setIdMovil(saveEstado.getIdMovil());
                        nEstado.setStatus(1);
                        dbEstados.update(nEstado);
                        sqlPhp.actualizarEstadoWebService(nEstado,nEstado.getId());
                        Log.e("",""+nEstado.getId());
                        Toast.makeText(MainActivity.this,"Modificado", Toast.LENGTH_SHORT).show();

                        limpiar();
                    }else{
                        nEstado.setNombre(txtSerie.getText().toString());
                        nEstado.setIdMovil(Device.getSecureId(MainActivity.this));
                        dbEstados.insert(nEstado);
                        sqlPhp.insertarEstadoWebService(nEstado);
                        Toast.makeText(MainActivity.this, "Estado agregado!", Toast.LENGTH_SHORT).show();
                        saveEstado = null;
                        limpiar();
                    }
                }
            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dbEstados.verificarLista() == false)
                {
                    Log.e(TAG, "onCreate: VACIO");
                    for (int i = 0; i < 10; i++)
                    {
                        estado.setId(i+1);
                        estado.setNombre("Mazatlan" + i+1);
                        estado.setStatus(1);
                        estado.setIdMovil(Device.getSecureId(MainActivity.this));
                        dbEstados.insert(estado);
                    }
                }else{
                    Log.e(TAG, "onCreate: VACIO2");
                }
                Intent i = new Intent(MainActivity.this, ListaActivity.class);
                Bundle bObject= new Bundle();
                bObject.putSerializable("contactos",estados);
                i.putExtras(bObject);
                startActivityForResult(i,0);
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
}
